import {createHeaders} from "./indexApi";

const apiUrl = process.env.REACT_APP_API_URL

const checkForUser = async (username) => {
    try {
        const response = await fetch(`${apiUrl}?username=${username}`)

        if(!response.ok) {
            throw new Error("Could not complete request")
        }
        const data = await response.json();

        return [null, data]

    } catch (error) {
        return [error.message, []]
    }
}

const createUser = async (username) => {
    try {
        const response = await fetch(`${apiUrl}`, {
            method: "POST",
            headers: createHeaders(),
            body: JSON.stringify({
                username,
                translations: []
            })
        });

        if(!response.ok) {
            throw new Error("Could not create user with username " + username)
        }
        const data = await response.json();

        return [null, data]

    } catch (error) {
        return [error.message, []]
    }
}

export const loginUser = async (username) => {
    const [checkError, user] = await checkForUser(username)

    if(checkError !== null) return [checkError, null]

    if(user.length > 0) {                                       //user exists
        return [ null, user.pop()]                  //user egy tömb, pop() eltávolítja belőle az utolsó(itt egy van benne, mert username-re kerestünk) elemet és visszaadja, így itt a user lesz
    }

    //ha idáig lejutottunk, user does not exist, we need to create new one:
    return  await createUser(username)          //vagy a user van benne, vagy a createError

}

export const userById = async (userId) => {
    try {
        const response = await fetch(`${apiUrl}/${userId}`)
        if(!response.ok) {
            throw new Error("Could not find user with id " + userId)
        }
        const user = await response.json();
        return [null, user]

    } catch (error) {
        return [error.message, null]
    }
}
