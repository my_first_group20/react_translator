import {NavLink} from "react-router-dom";
import {useUser} from "../../context/UserContext";

const Navbar = () => {

    const { user } = useUser();

    return (
        <nav className="navbar">
            <ul>
                <li className="headline">Lost In Translation</li>
            </ul>
            {/* csak akkor jelenjen meg, ha van bejelentkezett user: */}
            { user !== null &&
                <ul className="user-menu">
                    <li>
                        <NavLink to="/translation" className="user-menu-link">Translation</NavLink>
                    </li>
                    <li>
                        <NavLink to="/profile" className="user-menu-link">Profile</NavLink>
                    </li>
                </ul>
            }

        </nav>
    );
};

export default Navbar;
