import {Link} from "react-router-dom";
import {storageDelete, storageSave} from "../../utils/storage";
import {useUser} from "../../context/UserContext";
import {translationClearHistory} from "../../api/translationApi";

const ProfileActions = () => {

    const { user, setUser } = useUser();

    const handleLogoutClick = () => {
        const doLogout = window.confirm("Are you sure?")                 //Cancel vs OK, false/true lesz belőle
        if(doLogout) {
            storageDelete("user")
            setUser(null)
        }
    }

    const handleClearHistory = async () => {
        const surelyClear = window.confirm("Are you sure? \n This can't be reversed")
        if(!surelyClear) return

        const [ clearError, userCleared ] = await translationClearHistory(user)
        if(clearError !== null) return

        storageSave("user", userCleared);
        setUser(userCleared);
    }

    return (
        <div className="profile-btn-container">
            <button className="profile-btn" onClick={ handleClearHistory }>Clear history</button>
            <button className="profile-btn" onClick={ handleLogoutClick }>Logout</button>
        </div>
    );
};

export default ProfileActions;
