import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem";

const ProfileTranslationHistory = ({ translations }) => {

    const translationList = translations.map((translation, index) => {
        return (<ProfileTranslationHistoryItem key={ `${index}-${translation}` } translation = { translation }/>)
    })

    return (
        <section className="translation-history-container">
            <h4 className="translation-history-title">Translation History</h4>
            <ul>{ translationList }</ul>
        </section>
    );
};

export default ProfileTranslationHistory;
