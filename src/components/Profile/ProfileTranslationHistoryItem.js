const ProfileTranslationHistoryItem = ({ translation }) => {
    return (
        <li className="translation-history-item">{ translation }</li>
    );
};

export default ProfileTranslationHistoryItem;
