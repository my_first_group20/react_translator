const SignCharacter = ({ letter }) => {

    const path = "./signImg/";
    const src = require(path + letter + ".png");

    return (

            <img style={{ height: "50px", width: "50px" }} src={src} alt={letter}></img>

    );
};

export default SignCharacter;
