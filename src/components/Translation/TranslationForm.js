import {useForm} from "react-hook-form";
import {FaArrowCircleRight, FaRegKeyboard} from "react-icons/fa";

const TranslationForm = ({ onTranslation }) => {

    const { register, handleSubmit } = useForm()

    const onSubmit = formData => {
        onTranslation(formData.translationInput)
    }

    return (
        <form onSubmit={ handleSubmit(onSubmit) }>
                <label htmlFor="translation-input">Words to translate: </label>
            <div className="translation-input-container">
                <FaRegKeyboard className="keyboard-icon" />
                <span className="divider"> | </span>
                <input type="text" { ...register("translationInput") } placeholder="Type words to translate" className="translation-input-field" />
                <button type="submit" className="arrow-button"><FaArrowCircleRight className="arrow-icon"/></button>
            </div>

        </form>
    );
};

export default TranslationForm;
