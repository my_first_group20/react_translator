import SignCharacter from "../SignCharacter/SignCharacter";

const TranslationResult = ({ lettersToTranslate }) => {

    const signs = createSigns(lettersToTranslate);

    function createSigns(letters) {
        return lettersToTranslate
            .filter((char) => "a" <= char && char <= "z")
            .map((letter, index) => <SignCharacter letter={letter} key={index} />);
    }

    return (
        <div>
            { signs }
        </div>
    );
};

export default TranslationResult;
