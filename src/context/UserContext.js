import {createContext, useContext, useState} from "react";
import {storageRead} from "../utils/storage";

//Context object -> exposing
const UserContext = createContext();

export const useUser = () => {
    return useContext(UserContext)                  //returns an object with { user, setUser }
}


//Provider -> managing state
const UserProvider = (props) => {

    const [ user, setUser ] = useState(storageRead("user"))

    const state = {
        user,
        setUser
    }

    return (
        <UserContext.Provider value={ state }>
            { props.children }
        </UserContext.Provider>
    )
}
export default UserProvider
