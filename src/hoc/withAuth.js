import {useUser} from "../context/UserContext";
import { Navigate } from "react-router-dom"

//Higher Order Component
const withAuth = Component => props => {        //closure, függvényből return-ölt függvény

    const { user } = useUser()

    if(user !== null) {
        return <Component {...props} />
    } else {
        return <Navigate to="/" />
    }

};

export default withAuth;
