import withAuth from "../hoc/withAuth";
import ProfileActions from "../components/Profile/ProfileActions";
import ProfileTranslationHistory from "../components/Profile/ProfileTranslationHistory";
import {useUser} from "../context/UserContext";
import {useEffect} from "react";
import {userById} from "../api/userApi";
import {storageSave} from "../utils/storage";

const Profile = () => {

    const { user, setUser } = useUser();

    //todo: ez az egész nem kell? meghívása ki van kommentelve. megnézni egészet kikommentelve
    useEffect(() => {

        const findUser =async () => {
            const [error, latestUser] = await userById(user.id)
            if(error === null) {
                storageSave("user", latestUser)
                setUser(latestUser)
            }
        }

        //findUser()

    }, [setUser, user.id])

    return (
        <div className="profile-container">
            <h2 className="profile-headline">Welcome {user.username}!</h2>
            <ProfileActions/>
            <ProfileTranslationHistory translations={ user.translations }/>
        </div>


    );
};

export default withAuth(Profile);
