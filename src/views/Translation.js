import withAuth from "../hoc/withAuth";
import TranslationForm from "../components/Translation/TranslationForm";
import {useState} from "react";
import {useUser} from "../context/UserContext";
import {translationAdd} from "../api/translationApi";
import {storageSave} from "../utils/storage";
import TranslationResult from "../components/Translation/TranslationResult";


const Translation = () => {

    //local state
    const [ lettersToTranslate, setLettersToTranslate ] = useState();
    const { user, setUser } = useUser();

    const handleTranslationClicked = async (translationInput) => {

        console.log(translationInput)
        setLettersToTranslate(translationInput.toLowerCase().split(""))

        const [error, updatedUser] = await translationAdd(user, translationInput)

        if(error !== null ) return;

        //keep UI state and server state in sync
        storageSave("user", updatedUser)
        //update Context state
        setUser(updatedUser);

        console.log("Error: ", error)
        console.log("Updated user: ", updatedUser)
    }

    return (
        <>
            <section id="translation-form">
                <TranslationForm onTranslation={ handleTranslationClicked } />
            </section>
            {lettersToTranslate && <section  className="translation-result-container">
                                        <TranslationResult lettersToTranslate={ lettersToTranslate } />
                                    </section>}

        </>
    );
};

export default withAuth(Translation);

